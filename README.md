# WSL-Management
Powershell module that adds a couple of commandlets for managing WSL images

## Usage:

### Exporting an existing distribution
Exports an image to `$HOME\WSL Installer\Images\<NEW IMAGE NAME>`.
```powershell
Export-Distro
```
This command will list the current distributions installed in WSL and ask the
user to pick from the list.
The next screen asks for a distribution name. After that the new image will be
available for use by the Install-Distro commandlet.

### Installing a new distribution
Imports an image from `$HOME\WSL Installer\Images\<IMAGE NAME>`.
The default install location for new distributions will be
`$HOME\WSL\<NEW IMAGE NAME>`
```powershell
Install-Distro
```
This command will install one of the exported images from your computer. Current
working option is only local. In a future version you will be able to use a
remote repository.

### Removing a distribution
Removes an installed distro. This commandlet will also offer to clean up files
left behind by `wsl.exe`
```powershell
Remove-Distro
```
