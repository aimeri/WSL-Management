﻿<#
.SYNOPSIS
This script installs new distributions for your WSL

.DESCRIPTION
This script will download a base distribution configured for your
from the github repo configured in the script

.PARAMETER DistroName
Parameter Name of your distribution once installed. This is what
will show in your Terminal tabs.

.EXAMPLE
Install-Distro -DistroName HaskellDevelopment

.NOTES
The base images are hosted on a private repository. Configure
this script with your own settings to use your own base images
#>

#region Distro Installation
function Install-Distro {
    param (
        [string]$DistroName
    )
    Write-Prompt -Step "Install Distro" -Prompt "Local or remote repository?"
    Write-Host "Local Repository or remote?"
    Write-Host "1) Local"
    Write-Host "2) Remote - Not yet implemented"
    $selection = [System.Console]::ReadKey($true) | ForEach-Object { [int]($_.KeyChar.ToString()) }
    if ($selection -eq 1) {
        Install-Local
    } elseif ($selection -eq 2) {
        Read-Host "This function is not yet implemented."
        Install-Distro
       # Install-Remote
    } else {
        exit
    }

}
#endregion Distro Installation

#region Local Installer
function Install-Local {
    $DistrosAvailable = Get-ChildItem "$home\WSL Installer\Images\" | Select-Object Name
    Write-Prompt -Step "Install Distro" -Prompt "Select one of the available distros"
    For ($i = 0; $i -lt $DistrosAvailable.Length; $i++ ) {
        Write-Host "$($i + 1) ) " $DistrosAvailable[$i].name
    }
    $selection = [System.Console]::ReadKey($true) | ForEach-Object { [int]($_.KeyChar.ToString()) - 1 }
    $BaseDistroName = $DistrosAvailable[$selection].Name
    $BaseImageFolder = "$home\WSL Installer\Images\$BaseDistroName\$BaseDistroName"
    Write-Host "Installing new distro"
    $DistroName = Read-Host "Enter a name for the new Distro"
    $NewImageDestination = "$HOME\WSL\$DistroName"
    try {
        wsl.exe --import $DistroName $NewImageDestination $BaseImageFolder
    }
    Catch {
        Remove-Item –path $NewImageDestination –recurse
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Throw "Install Issue: $ErrorMessage $FailedItem"
    }
    try {
        # Set user to default created by ubuntu installer. Without this, imports
        # default to uid 0 which is root.
        Get-ChildItem -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Lxss |
        Get-ItemProperty | Where-Object { $_.DistributionName -match $DistroName } |
        Set-ItemProperty -name "DefaultUid" -value "1000"
    }
    Catch {
        Remove-Item –path $NewImageDestination –recurse
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Throw "Seting up current user Issue: $ErrorMessage $FailedItem"
    }
    Write-Host "Distro installed. Cleaning up"
    try {
        Remove-Item –path $NewImageDestination –recurse
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Throw "Cleaning up Issue: $ErrorMessage $FailedItem"
    }
    Write-host ""
    Write-Host "All done."
}
#endregion Local Install

#region Remote Installer
# WIP
#endregion Remote Installer

#region Export Distro
Function Export-Distro {
    Write-Prompt -Step "Export Distro" -Prompt "Please select a base image for the new Distro"
    $SelectedDistro = Get-DistroSelection
    Write-Prompt -Step "Export new Distro" -Prompt "Please give a name to the new Distro"
    $ExportLocation = "$HOME\WSL Installer\Images"
    $NewDistroName = Read-Host "Type the name of the new Distro"
    $NewDistroPath = "$HOME\WSL Installer\Images\$NewDistroName"
    $ImageLocation = "$NewDistroPath\$NewDistroName"
    try {
        Write-Prompt -Step "Export new Distro" -Prompt "Exporting $NewDistroName. Please wait."
        if(-not (Test-Path $NewDistroPath)){
            New-Item -Path $ExportLocation -Name $NewDistroName -ItemType "directory"
        }
        wsl --export $SelectedDistro $ImageLocation
    }
    catch {
        Write-Prompt -Step "Export new Distro" -Prompt "Exporting $NewDistroName failed"
        Write-Host "Cleaning up."
        Remove-Item –path $NewDistroPath –recurse
        Write-Host "All done. Please try again"
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Throw "WSL Exporting Issue: $ErrorMessage $FailedItem"
    }
    Write-Prompt -Step "Upload new Distro" -Prompt "Uploading $NewDistroName finished"
    Write-Host "All done."
}
#endregion Export Distro

#region Distro Removal
function Remove-Distro {
    Write-Prompt -Step "Remove Distro" -Prompt "Select one of the options to unregister"
    try {
        $SelectedDistro = Get-DistroSelection
        $DistroRegistryKey = Get-ChildItem -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Lxss | Get-ItemProperty | Where-Object { $_.DistributionName -match $SelectedDistro }
        $DistroPath = $DistroRegistryKey.BasePath.substring(4)
        Write-Prompt -Step "Remove Distro" -Prompt "Unregistering $SelectedDistro"
        wsl --unregister $SelectedDistro
        Write-Prompt -Step "Remove Distro" -Prompt "Cleaning up"
        Write-Host "Remove files? (y/n)"
        do {
            $selection = [System.Console]::ReadKey($true).KeyChar
        } while (($selection -notmatch "y") -and ($selection -notmatch 'n'))
        if ($selection -match 'y') {
            Write-Host "Removing Files"
            Remove-Item -LiteralPath $DistroPath
        }
        else {
            Write-Host "Files can be found here: " $DistroPath
        }
        Write-Host "Distro successfully removed"
    }
    Catch {
        $ErrorMessage = $_.Exception.Message
        $FailedItem = $_.Exception.ItemName
        Throw "WSL Removal Issue: $ErrorMessage $FailedItem"
    }
}

#endregion Distro Removal

#region Image Uploading

# WIP

#endregion Image Uploading

#region Utility Functions
function Get-WSLInstalledDistros {
    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = "wsl.exe"
    $pinfo.Arguments = "-l"
    $pinfo.UseShellExecute = $false
    $pinfo.CreateNoWindow = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.RedirectStandardError = $true
    $process = New-Object System.Diagnostics.Process
    $process.StartInfo = $pinfo
    $process.Start() | Out-Null
    Start-Sleep -Milliseconds 100
    if (!$process.HasExited) {
        $process.Kill()
    }
    $output = [string]$process.StandardOutput.ReadToEnd() -split '\n'
    Start-Sleep -Milliseconds 100
    return $output
}

function Get-DistroSelection {
    $InstalledDistros = Get-WSLInstalledDistros | Get-ParsedDistros
    For ($i = 0; $i -lt $InstalledDistros.Length; $i++ ) {
        Write-Host "$($i + 1) ) " $InstalledDistros[$i]
    }
    do {
        $selection = [System.Console]::ReadKey($true) | ForEach-Object { [int]($_.KeyChar.ToString()) - 1 }
    } while ($selection -notin 0..($InstalledDistros.length - 1))
    $SelectedDistro = $InstalledDistros[$selection]
    Return $SelectedDistro
}
function Write-Prompt {
    param (
        [Parameter(Mandatory = $True)]
        [string]$Step,

        [Parameter(Mandatory = $True)]
        [string]$Prompt

    )
    $spacesTop = [math]::Floor((78 - $Step.length) / 2)
    $spacesBottom = [math]::Floor((78 - $Prompt.length) / 2)
    $normalizedSpaceStep = 79 - (((" " * $spacesTop ).length) * 2 + 2 + $Step.Length)
    $normalizedSpacePrompt = 80 - (((" " * $spacesBottom ).length) * 2 + 2 + $Prompt.Length)
    $leftPaddingTop = "#" + (" " * ($spacesTop + 1))
    $rigthPaddingTop = (" " * ($spacesTop + $normalizedSpaceStep)) + "#"
    $leftPaddingBottom = "#" + (" " * $spacesBottom)
    $rigthPaddingBottom = (" " * ($spacesBottom + $normalizedSpacePrompt)) + "#"
    Get-Variable true | Out-Default; Clear-Host;
    Write-Host @"
$("#" * 80)
#$(" " * 30)WSL Management Tool$(" " * 29)#
#$(" " * 29)$("-" * 21)$(" " * 28)#
$leftPaddingTop$Step$rigthPaddingTop
$("#" * 80)
$leftPaddingBottom$Prompt$rigthPaddingBottom
$("#" * 80)

"@
}
function Get-ParsedDistros {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $True, ValueFromPipeline = $True)]
        [String[]]
        $WslRawText
    )
    process {
        $AvailableDistros = @()
        $nonAlphaNumerical = '[^0-9A-Za-z\-\.]|'
        $partial = $WslRawText -replace "`n", "" -replace "`r", ""
        $partial = [regex]::Escape($partial) -replace $nonAlphaNumerical, ''
        if ($partial -match '^(?:(?!docker).)*$' -and $partial -match '^(?:(?!WindowsSubsystemforLinuxDistributions).)*$') {
            $AvailableDistros += $partial
        }
        $AvailableDistros = $AvailableDistros.Where( { "$null" -ne $_ })
        return $AvailableDistros
    }
}
#endregion Utility Functions
